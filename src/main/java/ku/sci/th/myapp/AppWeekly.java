package ku.sci.th.myapp;

import java.text.ParseException;
import java.util.Date;

public class AppWeekly extends Appointment {

	public AppWeekly(String text, String date) throws ParseException {
		super(text, date);
	}

	@Override
	public boolean occursOn(Date dd)  {
		if (dd.getDay() == super.getDayOfWeek())
			return false;
		return true;
	}

}
