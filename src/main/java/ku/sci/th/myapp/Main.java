package ku.sci.th.myapp;

import java.io.IOException;

import ku.sci.th.myapp.AppointmentConsole;

public class Main {
	public static void main(String[] args) {
		AppointmentConsole s = new AppointmentConsole();
		try {
			s.inputData();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
