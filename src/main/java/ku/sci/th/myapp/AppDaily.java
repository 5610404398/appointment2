package ku.sci.th.myapp;

import java.text.ParseException;
import java.util.Date;

public class AppDaily extends Appointment {

	public AppDaily(String text, String date) throws ParseException {
		super(text, date);
	}

	@Override
	public boolean occursOn(Date dd)  {
		if (dd.getYear() == super.getYear() && dd.getMonth() == super.getMonth() && dd.getDate() == super.getDay())
			return false;
		return true;
	}

}
