package ku.sci.th.myapp;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class AppointmentBook {
	private ArrayList<Appointment> listall;
	private Appointment app;
	private int count;
	private String str;

	// private List lists;

	public AppointmentBook() {
		listall = new ArrayList<Appointment>();
		str = "";
	}

	public void addAll(List<Appointment> list) {
		listall.addAll(list);
	}

	public void add(Appointment apt) {
		listall.add(apt);
		count++;
	}

	public void add(String date, String desc) throws ParseException {
		listall.add(new Appointment(desc, date));
		count++;
	}

	public String getNumAppointments() {
		return count + "";
	}

	public String getAppointment(int index) {
		return listall.get(index).toString();
	}

	public ArrayList<Appointment> getlist() {
		return listall;
	}

	public boolean checkDateInList(Appointment app) {
		boolean check = true;
		for (int i = 0; i < listall.size(); i++) {
			if (check == true) {
				if (app.getClass().equals(listall.get(i).getClass())) {
					check = app.occursOn(listall.get(i).getDate());
				}
			}
		}
		return check;
	}

	public String toString() {
		for (int i = 0; i < listall.size(); i++) {
			str += listall.get(i).toString() + "\n";
		}
		return str;
	}
}
