package ku.sci.th.myapp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.ParseException;

public class AppointmentConsole {
	private String start = "y";
	private BufferedReader bfreader;
	private Appointment app;
	private AppointmentBook appb;

	public AppointmentConsole() {
		bfreader = new BufferedReader(new InputStreamReader(System.in));
		appb = new AppointmentBook();
	}

	public void inputData() throws IOException {
		while (start.equals("y")) {
			System.out.println(">> Please add your information <<");
			System.out.print("Type one time, daily, weekly, monthly : ");
			String type = bfreader.readLine();
			System.out.print("Date : ");
			String date = bfreader.readLine();
			System.out.print("Description : ");
			String des = bfreader.readLine();
			System.out.print("Do you want to add another information? (y/n): ");
			this.start = bfreader.readLine();

			try {
				if (type.equals("one time")) {
					app = new AppOnetime(des, date);
				}
				if (type.equals("daily")) {
					app = new AppDaily(des, date);
				}
				if (type.equals("weekly")) {
					app = new AppWeekly(des, date);
				}
				if (type.equals("monthly")) {
					app = new AppMonthly(des, date);
				}
				if (appb.checkDateInList(app)) {
					appb.add(app);
					System.out.println(app.getDate());
					System.out.println(app.toString());
				}
			} catch (ParseException e) {
				System.err.println("You add wrong date,please add again.");
				System.exit(0);

			}
		}
	}
}
